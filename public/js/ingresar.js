// Import the functions you need from the SDKs you need
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyC5vCgiX_-PKzUEDgdQNlEc4o5lXalMfgQ",
    authDomain: "forojere.firebaseapp.com",
    projectId: "forojere",
    storageBucket: "forojere.appspot.com",
    messagingSenderId: "260235713443",
    appId: "1:260235713443:web:9363672723055f3b70d816"
  };

//inicializo firebase 
const app = initializeApp(firebaseConfig);

const auth = getAuth();

console.log("Bienvenido a la consola de pruebas.");
// Serial.println("...");

var lcorreoRef = document.getElementById("lcorreoId");
//console.log(lcorreoRef);

var lpasswordRef = document.getElementById("lpasswordId");
//console.log(lpasswordRef);

var ingresarRef = document.getElementById("ingresarbotonId");

//llamo a las funciones con un evento
ingresarRef.addEventListener("click", logIn);

// Creo la funcion logIn
function logIn ()
{

    if((lcorreoRef.value != '') && (lpasswordRef.value != '')){

        signInWithEmailAndPassword(auth, lcorreoRef.value, lpasswordRef.value) 
        .then((userCredential) => {
            const user = userCredential.user;
             // si esta todo correcto se direcciona al usuario a la página de inicio
            window.location.href = "../index.html";
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revisar que los campos esten completos.");
    }    
}

