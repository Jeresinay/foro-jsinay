import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, onAuthStateChanged} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, push, get, remove, onValue, set} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyC5vCgiX_-PKzUEDgdQNlEc4o5lXalMfgQ",
    authDomain: "forojere.firebaseapp.com",
    projectId: "forojere",
    storageBucket: "forojere.appspot.com",
    messagingSenderId: "260235713443",
    appId: "1:260235713443:web:9363672723055f3b70d816"
  };

//inicializo firebase 
const app = initializeApp(firebaseConfig);
const auth = getAuth();
const database = getDatabase();


console.log("Bienvenido a la consola de pruebas.");
// Serial.println("...");

var registroRef = document.getElementById("registroid");
//console.log(registroRef);

var ingresoRef = document.getElementById("ingresoid");
//console.log(ingresoRef);

var enviarRef = document.getElementById("enviarbotonid");
//console.log(enviarRef);

var logoutRef = document.getElementById("logoutid");
//console.log(logoutRef);

var foroRef = document.getElementById("foroid");
//console.log(foroRef);

var cardRef = document.getElementById("cardinicioid");
//console.log(cardRef);

var mensajesRef = document.getElementById("mensajesid");
//console.log(mensajesRef);

var UserID;
var UserUID;
var usuario;

logoutRef.addEventListener("click", LogOut);

enviarRef.addEventListener("click", NuevoPost);



onAuthStateChanged(auth, (user) => {

    // solo se puede ver los botones para registrar e ingresar
    ingresoRef.style.display = "block";
    registroRef.style.display = "block";
    enviarRef.style.display = "none";
    cardRef.style.display = "block";
    logoutRef.style.display = "none";
  
    if (user) {
        usuario = user
        // una vez iniciado sesión, se puede ver el botón para cerrar sesión 
        ingresoRef.style.display = "none";
        registroRef.style.display = "none";
        enviarRef.style.display = "block";
        cardRef.style.display = "none";
        logoutRef.style.display = "block";
         
        get(ref(database, "users/" + user.uid)).then((snap) => {
          usuario = snap.val();
          UserID = usuario.Name;
          UserUID = user.uid;
        })
    }
});

function LogOut(){
    auth.signOut();
    location.reload(true);
}


function NuevoPost() {
    let message = foroRef.value;
    if (!message) return; 
    push(ref(database, "Mensajes/"), {
        autor: UserID,
        mensaje: message,
        ciudad: usuario.city,
        uid: UserUID,
    });
    foroRef.value = '';
} 

onValue(ref(database, 'Mensajes/'), (snapshot) => {
    const messages = snapshot.val();
    mensajesRef.innerHTML = '';
    
    for(const i in messages) {
      let message = messages[i];
      const campo_txt = document.createElement("div")
      campo_txt.classList.add('Mensaje')
      const p = document.createElement("p")
      p.classList.add('message-forum')
      p.innerText = message.autor + " (" + message.ciudad + ") " + ": " + message.mensaje;
      const Boton_Edit = document.createElement("button")
      Boton_Edit.classList.add('btn' , 'Boton_Editar')
      Boton_Edit.setAttribute("id", [i]);
      Boton_Edit.innerText = "Editar";
      const Boton_Clear = document.createElement("button")
      Boton_Clear.classList.add('btn' , 'Boton_Eliminar')
      Boton_Clear.setAttribute("id", [i]);
      Boton_Clear.innerText = "Eliminar";
  
      mensajesRef.appendChild(campo_txt).appendChild(p)
      const auth = getAuth();
      onAuthStateChanged(auth, (user) => {
      if (user) {
      const uid = user.uid;
      if(uid == message.uid){
        mensajesRef.appendChild(campo_txt).appendChild(Boton_Edit)
        mensajesRef.appendChild(campo_txt).appendChild(Boton_Clear)
        }
        else{
        mensajesRef.appendChild(campo_txt)
        mensajesRef.appendChild(campo_txt)
        }
      }
      });
      Boton_Clear.addEventListener("click", (e) => {
        Eliminar([i]);
      })
      Boton_Edit.addEventListener("click", (e) => {
        Editar([i]);
      })  
    };
});

function Editar(Identificacion){
  let message = foroRef.value;
  set(ref(database, "Mensajes/" + Identificacion),{
    autor: UserID,
    mensaje: message,
    ciudad: usuario.city,
    uid: UserUID,
    });

    foroRef.value = "";
}

function Eliminar(Identificacion)
{
  console.log("BORRANDO post: " + Identificacion);
  remove(ref(database, "Mensajes/" + Identificacion));
}


     
    

