// Import the functions you need from the SDKs you need
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyC5vCgiX_-PKzUEDgdQNlEc4o5lXalMfgQ",
    authDomain: "forojere.firebaseapp.com",
    projectId: "forojere",
    storageBucket: "forojere.appspot.com",
    messagingSenderId: "260235713443",
    appId: "1:260235713443:web:9363672723055f3b70d816"
  };

//inicializo firebase 
const app = initializeApp(firebaseConfig);

const database = getDatabase(app);
const auth = getAuth();

console.log("Bienvenido a la consola de pruebas.");
// Serial.println("...");

var rcorreoRef = document.getElementById("RdireccionCorreoId");
//console.log(rcorreoRef);

var rpasswordRef = document.getElementById("RpasswordId");
//console.log(rpasswordRef);

var rciudadRef = document.getElementById("RciudadId");
//console.log(rciudadRef);

var rnombreRef = document.getElementById("RapellidoNombreId");
//console.log(anombreRef);

var registrarRef = document.getElementById("registrarbotonId")

//llamo a las funciones con un evento
registrarRef.addEventListener("click", createUser);

function createUser()
{
    
    if((rnombreRef.value != '') && (rciudadRef.value != '') && (rcorreoRef.value != '') && (rpasswordRef.value != ''))
    {

        createUserWithEmailAndPassword(auth, rcorreoRef.value, rpasswordRef.value)
        .then((userCredential) => {
            // Guardo la credencial del usuario
            const user = userCredential.user;   
            DataBaseUser(user.uid)
        })

        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage); 
            if(errorCode == 'auth/email-already-in-use')
            {
                alert("E-mail ya en uso por otro usuario."); 
            }
        });
        
    }
    else
    {
       alert("Revisar que los campos esten completos"); // en caso de que ocurra un error en los campos
    }    
}

function DataBaseUser(uid) {
  
    let Email = rcorreoRef.value
    let Nombre = rnombreRef.value
    let Ciudad = rciudadRef.value
    
    const UserData = {
        uid: uid,
        Name: Nombre,
        Email: Email,
        city: Ciudad,
    };
    // una ves registrada la cuenta, pongo todo en blanco
    rnombreRef.value = ""
    rciudadRef.value = ""
    rcorreoRef.value = ""
    rpasswordRef.value = ""
        
    set(ref(database, "users/" + uid), UserData).then(() => {
        alert("Se registro la cuenta");
    });
}

